This is a backport for Delphi XE of what was https://github.com/bonecode/TBCEditor (before deletion & re-creation under a similar name by a different person)

Note that only Delphi XE is the only version supported in this backport, though the code will likely compile in older and more recent versions, no promises are made.